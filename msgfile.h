/*-----------------------------------------------------------------------
MsgEdit version 2.00 - Windows Federation 2 Message Editor
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef MSGFILE_H
#define MSGFILE_H

#include <map>
#include <string>

#include <QLabel>
#include <QString>
#include <QXmlDefaultHandler>

class Category;
class Section;

typedef	std::map<const QString, Category *>	CategoryMap;

class MsgFile : public QXmlDefaultHandler
{
public:
	enum	{ PARA_TEXT, COMMENT_TEXT };	// Which text we want to fetch

private:
	static const QString	el_names[];
	static const int		NOT_FOUND;

	QString	cat_name;
	QString	sect_name;
	QString	text;
	QString	comment;

	QLabel	*file_name_label;

	std::string	file_name;

	int	file_version;
	int	number;

	CategoryMap	category_map;

	// These three files are overrides of virtuals in QXmlDefaultHandler
	bool	characters(const QString&txt);
	bool	endElement(const QString& namespaceURI, const QString& localName, const QString& name);
	bool	startElement(const QString& namespaceURI, const QString& localName,
		const QString& qName, const QXmlAttributes& atts);

	int	FindElement(const QString& element);

	void	EndParsing();
	void	StartCategory(const QXmlAttributes& attribs);
	void	StartParaList(const QXmlAttributes& attribs);
	void	StartPara(const QXmlAttributes& attribs);
	void	StartSection(const QXmlAttributes& attribs);

public:
	static QString&	EscapeXML(QString& text);

	MsgFile();
	MsgFile(const std::string& path)					{ file_name = path; }
	~MsgFile();

	Section	*Find(const QString& cat_name, const QString sect_name);

	const std::string&	Name()						{ return file_name; }
	std::string	GetPara(const QString& category_name, const QString& section_name, int number, int which);

	int	GetNextParaNumber(const QString& category, const QString& section);

	bool	Add(const QString& category_name, const QString& section_name,
		int number, const QString& txt, const QString& fi_comment);
	bool	DeleteCategory(const QString& category_name);
	bool	DeletePara(const QString& category_name, const QString& section_name, int number);
	bool	DeleteSection(const QString& category_name, const QString& section_name);
	bool	Load();
	bool	Save();
	bool	Update(const QString& category_name, const QString& section_name,
		int number, const QString& txt, const QString& fi_comment);

	void	Clear();
	void	SetFileName(const std::string& path)	{ file_name = path; }
	void	WriteListing();
};

#endif
