/*-----------------------------------------------------------------------
MsgEdit version 2.00 - Windows Federation 2 Message Editor
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "category.h"

#include "msgedit.h"
#include "section.h"


Category::~Category()
{
	for (auto iter = section_map.begin(); iter != section_map.end(); ++iter)
		delete iter->second;
}

bool	Category::Add(const QString& section_name, int number, const QString& txt, const QString& fi_comment)
{
	Section	*section = nullptr;
	auto	iter = section_map.find(section_name);
	if (iter == section_map.end())
	{
		section = new Section(section_name);
		section_map.insert(std::make_pair(section_name, section));
	}
	else
		section = iter->second;
	return(section->Add(number, txt, fi_comment));
}

bool	Category::DeletePara(const QString& section_name, int number)
{
	auto	iter = section_map.find(section_name);
	if (iter == section_map.end())
		return false;
	else
		return(iter->second->Delete(number));
}

bool	Category::DeleteSection(const QString& section_name)
{
	auto	iter = section_map.find(section_name);
	if (iter == section_map.end())
		return false;
	else
	{
		auto temp = iter->second;
		section_map.erase(iter);
		delete temp;
		return true;
	}
}

Section	*Category::Find(const QString sect_name)
{
	auto	iter = section_map.find(sect_name);
	if (iter == section_map.end())
		return nullptr;
	else
		return(iter->second);
}

int	Category::GetNextParaNumber(const QString& section)
{
	auto iter = section_map.find(section);
	if (iter == section_map.end())
		return 1;
	else
		return(iter->second->GetNextParaNumber());
}

std::string	Category::GetPara(const QString& section_name, int number, int which)
{
	SectionMap::iterator iter = section_map.find(section_name);
	if (iter == section_map.end())
		return("");
	else
		return(iter->second->GetPara(number, which));

}

void	Category::Save(std::ofstream& file)
{
	QString temp = name.toLower();
	file << "   <category name='" << temp.toStdString() << "'>\n";
	for (auto iter = section_map.begin(); iter != section_map.end(); ++iter)
		iter->second->Save(file);
	file << "   </category>\n";
}

bool	Category::Update(const QString& section_name, int number, const QString& txt, const QString& fi_comment)
{
	auto	iter = section_map.find(section_name);
	if (iter == section_map.end())
		return false;
	else
		return(iter->second->Update(number, txt, fi_comment));
}

void	Category::WriteListing()
{
	MsgEdit::MainWindow()->DisplayListing(" ");
	MsgEdit::MainWindow()->DisplayListingBold((QString)("Category: ") + name);
	MsgEdit::MainWindow()->DisplayListing(" ");
	for (auto iter = section_map.begin(); iter != section_map.end(); ++iter)
		iter->second->WriteListing();
}

