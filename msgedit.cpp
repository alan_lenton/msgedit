/*-----------------------------------------------------------------------
MsgEdit version 2.00 - Windows Federation 2 Message Editor
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "msgedit.h"

#include <QDesktopServices>
#include <QFileDialog>
#include <QFontDialog>
#include <QMessageBox>
#include <QSettings>
#include <QTableWidgetItem>
#include <QTreeWidgetItemIterator>
#include <QUrl>

#include <cctype>

#include "msgfile.h"
#include "para.h"
#include "section.h"

MsgEdit	*MsgEdit::main_window = nullptr;
bool		MsgEdit::is_dirty = false;

MsgEdit::MsgEdit(QWidget *parent, Qt::WindowFlags flags) : QMainWindow(parent, flags)
{
	ui.setupUi(this);

	version = "2.09";
	is_dirty = false;

	// Set up right click menu in file tree & the message table
	tree_menu_action = new QAction(tr("Delete"), this);
	ui.file_tree->setContextMenuPolicy(Qt::ActionsContextMenu);
	ui.file_tree->addAction(tree_menu_action);
	mssg_table_action = new QAction(tr("Delete"), this);
	ui.mssg_table->setContextMenuPolicy(Qt::ActionsContextMenu);
	ui.mssg_table->addAction(mssg_table_action);

	SetUpConnections();
	QString title("Message Editor - Federation II - Version ");
	title += version;
	setWindowTitle(title);

	file_name_label = new QLabel;
	file_name_label->setIndent(3);
	statusBar()->addWidget(file_name_label);
	file_name_label->setText("No file selected");

	QCoreApplication::setOrganizationName("ibgames");
	QCoreApplication::setOrganizationDomain("ibgames.com");
	QCoreApplication::setApplicationName("MsgEdit");

	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames","MssgEdit");
	QFont font(settings.value("font-type", "verdana").toString(), settings.value("font-size", 9).toInt());
	font.setWeight(QFont::Normal);	// Fix to stop it coming back as bold every time
	ui.para_edit->setFont(font);
	resize(settings.value("win-size", QSize(780, 540)).toSize());
	move(settings.value("win-pos", QPoint(20, 20)).toPoint());
	data_dir = settings.value("data-dir", "./").toString();

	// Set up table
	ui.mssg_table->setColumnWidth(0, 24);
	ui.mssg_table->setColumnWidth(1, 400);

	ui.mssg_table->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.mssg_table->setAlternatingRowColors(true);

	msg_file = new MsgFile;
	main_window = this;
}

MsgEdit::~MsgEdit()
{
	QSettings	settings(QSettings::IniFormat,QSettings::UserScope,"ibgames","MssgEdit");
	settings.setValue("font-type", ui.para_edit->font().family());
	settings.setValue("font-size", ui.para_edit->font().pointSize());
	settings.setValue("win-size", size());
	settings.setValue("win-pos", pos());
	settings.setValue("data-dir", data_dir);

	delete msg_file;
}


void	MsgEdit::AboutSlot()
{
	QString	buffer = "Federation 2 Message Editor\n";
	buffer += "Copyright (c) 1985-2013 Alan Lenton\n";
	buffer += "Released under the GNU General Public License v3\n";
	buffer += "http://www.gnu.org/copyleft/gpl.html\n\n";
	buffer += "Design and programming by Alan Lenton\n";
	buffer += "Testing and manuals by Fiona Craig\n";
	buffer += "Website: www.ibgames.com\n";
	buffer += "Email: feedback@ibgames.com (include 'fed2' in the subject)\n\n";
	buffer += "git: https://bitbucket.org/alanxxx/msgeditor/overview";
	QMessageBox::information(this, "About Federation 2 Message Editor", buffer);
}

void	MsgEdit::AddParaClickedSlot()
{
	if (ui.para_edit->toPlainText().length() == 0)
	{
		QMessageBox::information(this, "Paragraphs", "You have not provided any text for the paragraph!");
		return;
	}

	if ((ui.category_edit->text() == "") || (ui.section_edit->text() == ""))
	{
		QMessageBox::information(this, "Paragraphs", "You must specify a Category and\na Section for the paragraph to go in.");
		return;
	}

	if (msg_file->Add(CleanText(ui.category_edit->text()), CleanText(ui.section_edit->text()),
		ui.number_spin->value(), ui.para_edit->toPlainText(), ui.comment_edit->text()))
	{
		DisplayInTable(ui.number_spin->value(), ui.para_edit->toPlainText());
		DisplayInTree(QString::fromStdString(msg_file->Name()), CleanText(ui.category_edit->text()), CleanText(ui.section_edit->text()));
		ui.comment_edit->clear();
		ui.para_edit->clear();
		ui.number_spin->setValue(ui.number_spin->value() + 1);
		ui.category_edit->setReadOnly(true);
		ui.section_edit->setReadOnly(true);
		is_dirty = true;
	}
}

void	MsgEdit::CatEditFinishedSlot()
{
	bool changed = false;
	QString	text(ui.category_edit->text());
	int len = ui.category_edit->text().length();
	for (int count = 0; count < len; ++count)
	{
		if (!std::isalnum(text[count].toLatin1()))
		{
			text.remove(count, 1);
			--len;
			changed = true;
		}
	}
	if (changed)
		ui.category_edit->setText(text);
}

void	MsgEdit::CellClickedSlot(int row, int column)
{
	QTableWidgetItem *item = ui.mssg_table->item(row, 0);
	int para_number = (item->text()).toInt();
	ui.para_edit->setPlainText(QString::fromStdString(msg_file->GetPara(ui.category_edit->text(), ui.section_edit->text(), para_number, MsgFile::PARA_TEXT)));
	ui.comment_edit->setText(QString::fromStdString(msg_file->GetPara(ui.category_edit->text(), ui.section_edit->text(), para_number, MsgFile::COMMENT_TEXT)));
	ui.number_spin->setValue(para_number);
}

QString MsgEdit::CleanText(const QString& txt)
{
	QString	input = txt;
	input = input.trimmed();
	input = input.replace('\r', ' ');
	input = input.replace('\n', ' ');
	input.remove(QChar('\''));
	input.remove(QChar('\"'));
	return input;
}

void	MsgEdit::ClearCatSecSlot()
{
	if ((ui.category_edit->text().length() == 0) && (ui.category_edit->text().length() == 0))
		return;

	if (ui.section_edit->text().length() == 0)
	{
		ui.category_edit->setText("");
		ui.category_edit->setReadOnly(false);
	}
	else
	{
		ui.section_edit->setText("");
		ui.section_edit->setReadOnly(false);
	}

	ui.number_spin->setValue(1);
	ui.file_tree->clearSelection();
	ui.mssg_table->clear();
	ui.mssg_table->setRowCount(0);
}

void MsgEdit::ClearData()
{
	ClearPara();
	ui.mssg_table->clear();
	ui.mssg_table->setRowCount(0);

	ui.file_tree->clear();
	ui.category_edit->clear();
	ui.section_edit->clear();
	ui.number_spin->setValue(1);
	ui.listing_edit->clear();
	ui.comment_edit->clear();
	ui.para_edit->clear();
	file_name_label->setText("No file selected");

	msg_file->Clear();
}

void	MsgEdit::ClearPara()
{
	ui.comment_edit->clear();
	ui.para_edit->clear();
	ui.chars_lcd->display(0);
	ui.char_progress->setValue(0);
}

void	MsgEdit::ClearTreeSelection()
{
	QTreeWidgetItemIterator iter(ui.file_tree);
	while (*iter)
	{
		(*iter)->setSelected(false);
		++iter;
	}
}

void MsgEdit::closeEvent(QCloseEvent * event)
{
	if (is_dirty)
	{
		int ret = QMessageBox::question(this, "File not saved", "Your work has not been saved.\nAre you sure you want to quit?", QMessageBox::Yes | QMessageBox::No);
		if (ret != QMessageBox::Yes)
			event->ignore();
		return;
	}
	event->accept();
}

void	MsgEdit::DeleteCategory(QTreeWidgetItem *category)
{
	QString message((QString)("Delete category '") + category->text(0) + "'\nAre you sure?");
	if (QMessageBox::question(this, "Delete Category", message, QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
		return;

	if (msg_file->DeleteCategory(category->text(0)))
	{
		delete category;
		ui.mssg_table->clear();
		ui.mssg_table->setRowCount(0);
		ui.category_edit->setReadOnly(false);
		ui.category_edit->setText("");
		ui.section_edit->setReadOnly(false);
		ui.section_edit->setText("");

		auto	selected_list = ui.file_tree->selectedItems();
		if (!selected_list.isEmpty())
			TreeClickedSlot(selected_list[0]);
	}
	else
		QMessageBox::warning(this, "Problem Detected", "I can't find the category you want to delete.\nPlease report the problem to feedback@ibgames.com.\nThank you.");
}

void	MsgEdit::DeleteParaSlot()
{
	if (msg_file->DeletePara(CleanText(ui.category_edit->text()), CleanText(ui.section_edit->text()), ui.number_spin->value()))
	{
		TreeClickedSlot(ui.file_tree->currentItem());
		is_dirty = true;
	}
}

void	MsgEdit::DeleteSecCatSlot()
{
	QList<QTreeWidgetItem *> selected_list = ui.file_tree->selectedItems();
	if ((selected_list.count() == 0) || (selected_list.isEmpty()))
	{
		QMessageBox::critical(this, "Delete  Category or Section", "You need to select a category\nor section in the File box first!");
		return;
	}

	if (selected_list[0]->type() == SECTION_ITEM)
		DeleteSection(selected_list[0]);
	else
		DeleteCategory(selected_list[0]);
}

void	MsgEdit::DeleteSection(QTreeWidgetItem *section)
{
	QString message((QString)("Delete section '") + section->text(0) + "'\nAre you sure?");
	if (QMessageBox::question(this, "Delete Section", message, QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
		return;

	QTreeWidgetItem	*parent = section->parent();
	QString	cat_name(parent->text(0));
	if (msg_file->DeleteSection(parent->text(0), section->text(0)))
	{
		delete section;
		ui.mssg_table->clear();
		ui.mssg_table->setRowCount(0);
		ui.section_edit->setText("");
		ui.section_edit->setReadOnly(false);

		auto	selected_list = ui.file_tree->selectedItems();
		if (!selected_list.isEmpty())
			TreeClickedSlot(selected_list[0]);
	}
	else
		QMessageBox::warning(this, "Problem Detected", "I can't find the section you want to delete.\nPlease report the problem to feedback@ibgames.com.\nThank you.");
}

void	MsgEdit::DisplayFileName(QString file_name)
{
	file_name_label->setText(file_name);
	SetDataDirectory(file_name);
}

void	MsgEdit::DisplayInTable(int number, const QString& text)
{
	int	rows = ui.mssg_table->rowCount();
	for (int count = 0; count < rows; ++count)
	{
		QTableWidgetItem *cur_item = ui.mssg_table->item(count, 0);
		if (cur_item == nullptr)
		{
			ui.mssg_table->removeRow(count);
			continue;
		}
		if (cur_item->text() == QString::number(number))
		{
			ui.mssg_table->item(count, 1)->setText(text);
			return;
		}
	}

	rows = ui.mssg_table->rowCount();	// Need this in case we have removed some rows
	ui.mssg_table->insertRow(rows);
	ui.mssg_table->setRowHeight(rows, 24);
	QTableWidgetItem	*item0 = new QTableWidgetItem;
	ui.mssg_table->setItem(rows, 0, item0);
	ui.mssg_table->item(rows, 0)->setData(Qt::EditRole, number);
	QTableWidgetItem	*item1 = new QTableWidgetItem;
	ui.mssg_table->setItem(rows, 1, item1);
	ui.mssg_table->item(rows, 1)->setText(text);
	ui.mssg_table->sortItems(0, Qt::AscendingOrder);
}

void	MsgEdit::DisplayInTree(const QString& file, const QString& category, const QString& section)
{
	//	Check for existing cat/sect...
	QTreeWidgetItemIterator cat_iter(ui.file_tree, QTreeWidgetItemIterator::HasChildren);
	while (*cat_iter)
	{
		if ((*cat_iter)->text(0) == category)
		{
			QTreeWidgetItemIterator sect_iter(ui.file_tree, QTreeWidgetItemIterator::NoChildren);
			while (*sect_iter)
			{
				if (((*sect_iter)->text(0) == section) && ((*sect_iter)->parent() == *cat_iter))
					return;	// already in tree
				++sect_iter;
			}
			// ...category is in tree, but it's a new section
			QTreeWidgetItem	*sect = new QTreeWidgetItem(*cat_iter, SECTION_ITEM);
			sect->setText(0, section);
			(*cat_iter)->setExpanded(true);
			ClearTreeSelection();
			sect->setSelected(true);
			return;
		}
		++cat_iter;
	}

	// ...it's a new section and category
	QTreeWidgetItem	*cat = new QTreeWidgetItem(ui.file_tree, CATEGORY_ITEM);
	cat->setText(0, category);
	cat->setExpanded(true);
	QTreeWidgetItem	*sect = new QTreeWidgetItem(cat, SECTION_ITEM);
	sect->setText(0, section);
	ClearTreeSelection();
	sect->setSelected(true);
}

void	MsgEdit::DisplayListing(QString text)
{
	ui.listing_edit->appendPlainText(text);
}

void	MsgEdit::DisplayListingBold(QString text)
{
	ui.listing_edit->appendHtml((QString)("<b>") + text + "</b>");
}

void	MsgEdit::DisplayStatusMessage(QString message)
{
	statusBar()->showMessage(message, 3000);
}

void	MsgEdit::ExitSlot()
{
	close();
}

void	MsgEdit::FontSlot()
{
	bool ok;
	QFont	original_font(ui.para_edit->font());
	QFont font = QFontDialog::getFont(&ok, ui.para_edit->font(), this);
	if (ok)
		ui.para_edit->setFont(font);
	else
		font = original_font;
}

bool MsgEdit::GetConfirmation(const std::string& message)
{
	int ret = QMessageBox::warning(this, "Confirmation", QString::fromStdString(message), QMessageBox::Yes | QMessageBox::Cancel);
	if (ret == QMessageBox::Yes)
		return true;
	else
		return false;
}

void	MsgEdit::HelpSlot()
{
	QUrl	url("http://www.ibgames.net/fed2/workbench/manual/index.html");
	QDesktopServices::openUrl(url);
}

void	MsgEdit::LoadSlot()
{
	if (is_dirty)
	{
		int ret = QMessageBox::question(this, "File not saved", "Your work has not been saved.\nDo you want to continue?", QMessageBox::Yes | QMessageBox::No);
		if (ret == QMessageBox::No)
			return;
	}

	QString	file_name = QFileDialog::getOpenFileName(this, tr("Open Message File"), data_dir, tr("Message Files (*.msg);;All (*.*)"));
	if (file_name != "")
	{
		ClearData();
		msg_file = new MsgFile(file_name.toStdString());
		if (msg_file->Load())
			is_dirty = false;
		ui.file_tree->clearSelection();
	}
}

void	MsgEdit::NewSlot()
{
	if (is_dirty)
	{
		int ret = QMessageBox::question(this, "File not saved", "Your work has not been saved.\nDo you want to continue?", QMessageBox::Yes | QMessageBox::No);
		if (ret == QMessageBox::No)
			return;
	}
	ClearData();
	ui.category_edit->setReadOnly(false);
	ui.category_edit->clear();
	ui.section_edit->setReadOnly(false);
	ui.section_edit->clear();
	is_dirty = false;
}

void	MsgEdit::NextNumBtnSlot()
{
	SetNextParaNumber();
}

void	MsgEdit::RowChangedSlot()
{
	CellClickedSlot(ui.mssg_table->currentRow(), ui.mssg_table->currentColumn());
}

void	MsgEdit::SaveSlot()
{
	if (msg_file->Save())
		is_dirty = false;
}

void	MsgEdit::SaveAsSlot()
{
	QString	file_name = QFileDialog::getSaveFileName(this, tr("Save File"), data_dir, tr("Message Files (*.msg);;All files(*.*)"));
	if (!file_name.isNull())
	{
		msg_file->SetFileName(file_name.toStdString());
		msg_file->Save();
	}
}

void	MsgEdit::SectionEditFinishedSlot()
{
	bool changed = false;
	QString	text(ui.section_edit->text());
	int len = ui.section_edit->text().length();
	for (int count = 0; count < len; ++count)
	{
		if (!std::isalnum(text[count].toLatin1()))
		{
			text.remove(count, 1);
			--len;
			changed = true;
		}
	}
	if (changed)
		ui.section_edit->setText(text);
}

void	MsgEdit::SetDataDirectory(QString file_name)
{
	int index = file_name.lastIndexOf('/');	// *nix/Qt native separator
	if (index == -1)
		index = file_name.lastIndexOf('\\');	// Try Windows separator
	if (index == -1)
		data_dir = "./";								// Give up and set to current directoty

	data_dir = file_name.remove(index, 999);
}

void	MsgEdit::SetFileTreeSelection(QString& cat_name, QString& sec_name)
{
	QList<QTreeWidgetItem *> cat_list = ui.file_tree->findItems(cat_name, Qt::MatchFixedString);
	QTreeWidgetItem *cat_item = nullptr;
	for (auto iter = cat_list.begin(); iter != cat_list.end(); ++iter)
	{
		if ((*iter)->type() == CATEGORY_ITEM) // Category names are unique
		{
			cat_item = *iter;
			break;
		}
	}
	if (cat_item == nullptr)	// can't find a cetegory with that name
		return;

	QList<QTreeWidgetItem *> sec_list = ui.file_tree->findItems(sec_name, Qt::MatchFixedString);
	QTreeWidgetItem *sec_item = nullptr;
	for (auto iter = sec_list.begin(); iter != sec_list.end(); ++iter)
	{
		if ((*iter)->parent() == cat_item)	// Section names are unique within a category
		{
			sec_item = *iter;
			break;
		}
	}
	if (sec_item == nullptr)
		return;

	ui.file_tree->setCurrentItem(sec_item);
}

void	MsgEdit::SetNextParaNumber()
{
	if ((ui.category_edit->text() == "") || (ui.section_edit->text() == ""))
		ui.number_spin->setValue(1);
	else
		ui.number_spin->setValue(msg_file->GetNextParaNumber(ui.category_edit->text(), ui.section_edit->text()));
}

void	MsgEdit::SetUpConnections()
{
	// main menu action connects
	connect(ui.about_action, SIGNAL(triggered()), this, SLOT(AboutSlot()));
	connect(ui.exit_action, SIGNAL(triggered()), this, SLOT(ExitSlot()));
	connect(ui.delete_para_action, SIGNAL(triggered()), this, SLOT(DeleteParaSlot()));
	connect(ui.delete_sec_cat_action, SIGNAL(triggered()), this, SLOT(DeleteSecCatSlot()));
	connect(ui.font_action, SIGNAL(triggered()), this, SLOT(FontSlot()));
	connect(ui.load_action, SIGNAL(triggered()), this, SLOT(LoadSlot()));
	connect(ui.help_manual_action, SIGNAL(triggered()), this, SLOT(HelpSlot()));
	connect(ui.new_action, SIGNAL(triggered()), this, SLOT(NewSlot()));
	connect(ui.save_action, SIGNAL(triggered()), this, SLOT(SaveSlot()));
	connect(ui.save_as_action, SIGNAL(triggered()), this, SLOT(SaveAsSlot()));

	// other connects
	connect(ui.para_edit, SIGNAL(textChanged()), this, SLOT(TextChangedSlot()));
	connect(ui.add_para_btn, SIGNAL(clicked(bool)), this, SLOT(AddParaClickedSlot()));
	connect(ui.category_edit, SIGNAL(editingFinished()), this, SLOT(CatEditFinishedSlot()));
	connect(ui.clear_cat_sec_btn, SIGNAL(clicked(bool)), this, SLOT(ClearCatSecSlot()));
	connect(ui.file_tree, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(TreeClickedSlot(QTreeWidgetItem*)));
	connect(ui.mssg_table, SIGNAL(cellClicked(int, int)), this, SLOT(CellClickedSlot(int, int)));
	connect(ui.update_para_btn, SIGNAL(clicked(bool)), this, SLOT(UpdateParaSlot()));
	connect(ui.main_tab, SIGNAL(currentChanged(int)), this, SLOT(UpdateListingSlot(int)));
	connect(ui.next_num_btn, SIGNAL(clicked(bool)), this, SLOT(NextNumBtnSlot()));
	connect(tree_menu_action, SIGNAL(triggered()), this, SLOT(DeleteSecCatSlot()));
	connect(ui.section_edit, SIGNAL(editingFinished()), this, SLOT(SectionEditFinishedSlot()));
	connect(mssg_table_action, SIGNAL(triggered()), this, SLOT(DeleteParaSlot()));

	connect(ui.mssg_table, SIGNAL(itemSelectionChanged()), this, SLOT(RowChangedSlot()));
}

void	MsgEdit::TextChangedSlot()
{
	QString	text(ui.para_edit->toPlainText());
	int	num_chars = text.length();
	if (num_chars > Para::MAX_TEXT_SIZE)
	{
		text.truncate(Para::MAX_TEXT_SIZE);
		num_chars = Para::MAX_TEXT_SIZE;
		ui.para_edit->setPlainText(text);
		ui.para_edit->moveCursor(QTextCursor::End);
	}
	ui.chars_lcd->display(num_chars);
	ui.char_progress->setValue(num_chars);
	is_dirty = true;
}

void	MsgEdit::TreeClickedSlot(QTreeWidgetItem *section_item)
{
	if (section_item->type() == CATEGORY_ITEM)
		return;

	QTreeWidgetItem *cat_item = section_item->parent();

	Section	*section = msg_file->Find(cat_item->text(0), section_item->text(0));
	if (section != nullptr)
	{
		ui.mssg_table->clear();
		ui.mssg_table->setRowCount(0);

		ClearPara();
		ui.category_edit->setText(cat_item->text(0));
		ui.section_edit->setText(section_item->text(0));

		auto para_map = section->GetParasAvalable();
		for (auto para = para_map->begin(); para != para_map->end(); ++para)
			DisplayInTable(para->first, para->second->Text());
		SetNextParaNumber();
		ui.category_edit->setReadOnly(true);
		ui.section_edit->setReadOnly(true);
	}
}

void	MsgEdit::UpdateListingSlot(int index)
{
	if (index == 0)
		return;

	ui.listing_edit->clear();
	msg_file->WriteListing();
}

void	MsgEdit::UpdateParaSlot()
{
	QString cat_name(CleanText(ui.category_edit->text()));
	QString sec_name(CleanText(ui.section_edit->text()));

	if (msg_file->Update(cat_name, sec_name, ui.number_spin->value(),
		ui.para_edit->toPlainText(), ui.comment_edit->text()))
	{
		DisplayInTable(ui.number_spin->value(), ui.para_edit->toPlainText());
		SetFileTreeSelection(cat_name, sec_name);
		ClearPara();
		is_dirty = true;
	}
}



