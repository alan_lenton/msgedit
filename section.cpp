/*-----------------------------------------------------------------------
MsgEdit version 2.00 - Windows Federation 2 Message Editor
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "section.h"

#include "msgedit.h"
#include "msgfile.h"
#include "para.h"

Section::~Section()
{
	for (auto iter = para_map.begin(); iter != para_map.end(); ++iter)
		delete iter->second;
}


bool	Section::Add(int number, const QString& txt, const QString& fi_comment)
{
	auto	iter = para_map.find(number);
	if (iter != para_map.end())
	{
		if (!MsgEdit::MainWindow()->GetConfirmation("Paragraph already exists. Overwrite it?"))
			return false;
		else
			Delete(number);
	}
	Para	*para = new Para(number, txt, fi_comment);
	para_map.insert(std::make_pair(number, para));
	return true;
}

bool	Section::Delete(int number)
{
	auto	iter = para_map.find(number);
	if (iter != para_map.end())
	{
		auto temp = iter->second;
		para_map.erase(iter);
		delete temp;
		return true;
	}
	else
		return false;
}

int	Section::GetNextParaNumber()
{
	if (para_map.size() == 0)
		return 1;
	else
		return(para_map.rbegin()->first + 1);
}

std::string	Section::GetPara(int number, int which)
{
	ParaMap::iterator iter = para_map.find(number);
	if (iter == para_map.end())
		return("");
	else
	{
		if (which == MsgFile::PARA_TEXT)
			return(iter->second->Text().toStdString());
		else
			return(iter->second->Comment().toStdString());
	}
}

void	Section::Save(std::ofstream& file)
{
	QString temp = name.toLower();
	file << "      <section name='" << temp.toStdString() << "'>\n";
	for (auto iter = para_map.begin(); iter != para_map.end(); ++iter)
		iter->second->Save(file);
	file << "      </section>\n";
}

bool	Section::Update(int number, const QString& txt, const QString& fi_comment)
{
	auto	iter = para_map.find(number);
	if (iter == para_map.end())
		return false;

	Delete(number);
	Para	*para = new Para(number, txt, fi_comment);
	para_map.insert(std::make_pair(number, para));
	return true;
}

void	Section::WriteListing()
{
	MsgEdit::MainWindow()->DisplayListingBold((QString)("Section: ") + name);
	MsgEdit::MainWindow()->DisplayListing(" ");
	for (auto iter = para_map.begin(); iter != para_map.end(); ++iter)
		iter->second->WriteListing();
}

