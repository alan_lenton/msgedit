/*-----------------------------------------------------------------------
MsgEdit version 2.00 - Windows Federation 2 Message Editor
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef PARA_H
#define PARA_H

#include <fstream>
#include <string>

#include	<QString>

class	Para
{
public:
	// Max chars in a comment is 32 - coded into msgedit.ui
	static const int	MAX_TEXT_SIZE = 440;		// Max chars in a paragraph

private:
	int		number;
	QString	comment;
	QString	text;

public:
	Para()							{ number = 0; }
	Para(int num, const QString& txt, const QString& fi_comment);
	~Para()							{		}

	QString	Comment() const	{ return comment; }
	QString	Text() const		{ return text; }
	QString	CleanText(const QString& txt);

	int	Number()					{ return number; }

	void	Save(std::ofstream& file);
	void	WriteListing();
};
#endif

