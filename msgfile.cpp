/*-----------------------------------------------------------------------
MsgEdit version 2.00 - Windows Federation 2 Message Editor
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "msgfile.h"

#include <fstream>
#include <sstream>

#include <cctype>

#include <QFile>
#include <QXmlInputSource>
#include <QXmlSimpleReader>

#include "category.h"
#include "msgedit.h"

MsgFile::MsgFile()
{
	file_name = "";
	file_version = 1;
}

MsgFile::~MsgFile()
{
	for (auto iter = category_map.begin(); iter != category_map.end(); ++iter)
		delete iter->second;
}


bool	MsgFile::Add(const QString& category_name, const QString& section_name,
	int number, const QString& txt, const QString& fi_comment)
{
	Category	*category = nullptr;

	auto	iter = category_map.find(category_name);
	if (iter == category_map.end())
	{
		category = new Category(category_name);
		category_map.insert(std::make_pair(category_name, category));
	}
	else
		category = iter->second;

	if (!category->Add(section_name, number, txt, fi_comment))
		return false;
	MsgEdit::MainWindow()->DisplayInTree(QString::fromStdString(file_name), category_name, section_name);
	return true;
}

void	MsgFile::Clear()
{
	for (auto iter = category_map.begin(); iter != category_map.end(); ++iter)
		delete iter->second;
	category_map.clear();

	file_name = "";
}

bool	MsgFile::DeleteCategory(const QString& category_name)
{
	auto	iter = category_map.find(category_name);
	if (iter == category_map.end())
		return false;
	else
	{
		auto temp = iter->second;
		category_map.erase(iter);
		delete temp;
		return true;
	}
}

bool	MsgFile::DeletePara(const QString& category_name, const QString& section_name, int number)
{
	auto	iter = category_map.find(category_name);
	if (iter == category_map.end())
		return false;
	else
		return(iter->second->DeletePara(section_name, number));
}

bool	MsgFile::DeleteSection(const QString& category_name, const QString& section_name)
{
	auto	iter = category_map.find(category_name);
	if (iter == category_map.end())
		return false;
	else
		return(iter->second->DeleteSection(section_name));
}

Section	*MsgFile::Find(const QString& cat_name, const QString sect_name)
{
	auto	iter = category_map.find(cat_name);
	if (iter == category_map.end())
		return nullptr;
	else
		return(iter->second->Find(sect_name));
}

std::string	MsgFile::GetPara(const QString& category_name, const QString& section_name, int number, int which)
{
	CategoryMap::iterator iter = category_map.find(category_name);
	if (iter == category_map.end())
		return("");
	else
		return(iter->second->GetPara(section_name, number, which));
}

int	MsgFile::GetNextParaNumber(const QString& category, const QString& section)
{
	auto iter = category_map.find(category);
	if (iter == category_map.end())
		return 1;
	else
		return(iter->second->GetNextParaNumber(section));
}

bool	MsgFile::Load()
{
	QFile	file(QString::fromStdString(file_name));
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return(false);

	QXmlInputSource	source(&file);
	QXmlSimpleReader	reader;
	reader.setContentHandler(this);
	if (!reader.parse(source))
		return false;
	file.close();
	MsgEdit::MainWindow()->DisplayFileName(QString::fromStdString(file_name));
	return true;
}

bool MsgFile::Save()
{
	MsgEdit *main_window = MsgEdit::MainWindow();
	if (file_name == "")
		main_window->SaveAsSlot();
	else
	{
		std::ofstream	file(file_name.c_str(), std::ios::out | std::ios::trunc);
		if (!file)
		{
			main_window->DisplayStatusMessage("Unable to open file for writing!");
			return false;
		}

		main_window->DisplayStatusMessage("Saving File...");
		file << "<?xml version=\"1.0\"?>\n\n<para-list version='" << file_version++;
		file << "' editor='standard'>\n";
		for (auto iter = category_map.begin(); iter != category_map.end(); ++iter)
			iter->second->Save(file);
		file << "</para-list>\n";

		main_window->DisplayFileName(QString::fromStdString(file_name));
		main_window->DisplayStatusMessage("File Saved to disk");
	}
	return true;
}

bool	MsgFile::Update(const QString& category_name, const QString& section_name,
	int number, const QString& txt, const QString& fi_comment)
{
	auto	iter = category_map.find(category_name);
	if (iter == category_map.end())
		return false;

	return(iter->second->Update(section_name, number, txt, fi_comment));
}

void	MsgFile::WriteListing()
{
	for (auto iter = category_map.begin(); iter != category_map.end(); ++iter)
		iter->second->WriteListing();
}


/*--------------------------------------
XML (.msg) file parsing etc functions
-----------------------------------*/

const QString	MsgFile::el_names[] = { "para-list", "category", "section", "para", "" };
const int		MsgFile::NOT_FOUND = 99;

bool	MsgFile::characters(const QString& txt)
{
	text += txt;
	return true;
}

bool	MsgFile::endElement(const QString& namespaceURI, const QString& localName, const QString& name)
{
	switch (FindElement(name))
	{
	case	0:	EndParsing();	break;
	case	1:	break;
	case	2:	break;
	case	3:	Add(cat_name, sect_name, number, text, comment);		break;
	}
	return true;
}

void	MsgFile::EndParsing()
{
	cat_name = "";
	sect_name = "";
	text = "";
	comment = "";
	number = 0;
}

int	MsgFile::FindElement(const QString& element)
{
	for (int which = 0; el_names[which] != ""; ++which)
	{
		if (element == el_names[which])
			return(which);
	}
	return(NOT_FOUND);
}

void	MsgFile::StartCategory(const QXmlAttributes& attribs)
{
	cat_name = attribs.value(QString::fromLatin1("name"));
}

bool	MsgFile::startElement(const QString& namespaceURI, const QString& localName,
	const QString& qName, const QXmlAttributes& atts)
{
	switch (FindElement(qName))
	{
	case	0:	StartParaList(atts);		break;
	case	1:	StartCategory(atts);		break;
	case	2:	StartSection(atts);		break;
	case	3:	StartPara(atts);			break;
	default: return(false);
	}
	return true;
}

void	MsgFile::StartParaList(const QXmlAttributes& attribs)
{
	file_version = attribs.value(QString::fromLatin1("version")).toInt();
	if (file_version == 0)
		file_version = 1;
	++file_version;
}

void	MsgFile::StartPara(const QXmlAttributes& attribs)
{
	number = attribs.value(QString::fromLatin1("number")).toInt();
	comment = attribs.value(QString::fromLatin1("comment"));
	text = "";
}


void	MsgFile::StartSection(const QXmlAttributes& attribs)
{
	sect_name = attribs.value(QString::fromLatin1("name"));
}

QString&	MsgFile::EscapeXML(QString& text)
{
	static QString ret_val;

	std::ostringstream	xml_buffer;
	std::string	buffer(text.toStdString());
	int	len = buffer.length();
	for (int count = 0; count <len; ++count)
	{
		switch (buffer[count])
		{
		case  '<':	xml_buffer << "&lt;";		break;
		case  '>':	xml_buffer << "&gt;";		break;
		case  '&':	xml_buffer << "&amp;";	break;
		case '\'':	xml_buffer << "&apos;";	break;
		case '\"':	xml_buffer << "&quot;";	break;
		default:	if (isascii(buffer[count]) == 0)
			xml_buffer << '#';
					else
						xml_buffer << buffer[count];
			break;
		}
	}

	ret_val = QString::fromStdString(xml_buffer.str());
	return ret_val;
	//	text = QString::fromStdString(xml_buffer.str());
	//	return text;
}

