/*-----------------------------------------------------------------------
MsgEdit version 2.00 - Windows Federation 2 Message Editor
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "para.h"

#include "msgedit.h"
#include "msgfile.h"

Para::Para(int num, const QString& txt, const QString& fi_comment)
{
	number = num;
	text = CleanText(txt);
	comment = CleanText(fi_comment);
}

QString Para::CleanText(const QString& txt)
{
	QString	input(txt);
	input = input.trimmed();
	input = input.replace('\r', ' ');
	input = input.replace('\n', ' ');
	return input;
}

void	Para::Save(std::ofstream& file)
{
	file << "         <para number='" << number << "' comment='" << MsgFile::EscapeXML(comment).toStdString() << "'>\n";
	file << "            " << MsgFile::EscapeXML(text).toStdString() << "\n";
	file << "         </para>\n";
}

void	Para::WriteListing()
{
	QString	buffer(QString::number(number) + ". [");
	buffer.append(comment + ("]"));
	MsgEdit::MainWindow()->DisplayListing(buffer);
	MsgEdit::MainWindow()->DisplayListing(text + "\n");
}