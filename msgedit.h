/*-----------------------------------------------------------------------
MsgEdit version 2.00 - Windows Federation 2 Message Editor
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef MSGEDIT_H
#define MSGEDIT_H

#include <QAction>
#include <QString>
#include <QTreeWidgetItem>

#include "ui_msgedit.h"

class	MsgFile;
class Section;

class MsgEdit : public QMainWindow
{
	Q_OBJECT

public:
	static MsgEdit *MainWindow()	{ return main_window; }
	static bool		IsDirty()		{ return is_dirty; }

private:
	static MsgEdit *main_window;
	static bool		is_dirty;

	enum	{ CATEGORY_ITEM = QTreeWidgetItem::UserType, SECTION_ITEM };

	Ui::MsgEditClass ui;

	QAction	*mssg_table_action;
	QAction	*tree_menu_action;
	QLabel	*file_name_label;
	QString	data_dir;
	QString	version;
	QTreeWidgetItem *parent;

	MsgFile	*msg_file;

	Section	*Find(const QString& cat, const QString sect);

	void	ClearData();
	void	ClearPara();
	void	ClearTreeSelection();
	void	DeleteCategory(QTreeWidgetItem *category);
	void	DeleteSection(QTreeWidgetItem *section);
	void	DisplayInTable(int number, const QString& text);
	void	SetDataDirectory(QString file_name);
	void	SetNextParaNumber();
	void	SetUpConnections();

	private slots:
	void	AboutSlot();
	void	AddParaClickedSlot();
	void	CellClickedSlot(int row, int column);
	void	ClearCatSecSlot();
	void	DeleteParaSlot();
	void	DeleteSecCatSlot();
	void	ExitSlot();
	void	FontSlot();
	void	HelpSlot();
	void	LoadSlot();
	void	NewSlot();
	void	NextNumBtnSlot();
	void	RowChangedSlot();
	void	SaveSlot();
	void	TextChangedSlot();
	void	TreeClickedSlot(QTreeWidgetItem *section_item);
	void	UpdateListingSlot(int index);
	void	UpdateParaSlot();

	void	CatEditFinishedSlot();
	void	SectionEditFinishedSlot();

protected:
	void closeEvent(QCloseEvent * event);

	public slots:
	void	SaveAsSlot();

public:
	static QString CleanText(const QString& txt);

	MsgEdit(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~MsgEdit();

	const QString& Version()	{ return version; }

	bool	GetConfirmation(const std::string& message);

	void	DisplayFileName(QString file_name);
	void	DisplayInTree(const QString& file, const QString& category, const QString& section);
	void	DisplayListing(QString text);
	void	DisplayListingBold(QString text);
	void	DisplayStatusMessage(QString message);
	void	SetFileTreeSelection(QString& cat_name, QString& sec_name);
};

#endif
