/*-----------------------------------------------------------------------
MsgEdit version 2.00 - Windows Federation 2 Message Editor
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef SECTION_H
#define SECTION_H

#include <fstream>
#include <map>
#include <memory>
#include <string>

#include <QString>

class Para;

typedef	std::map<int, Para *>	ParaMap;

class Section
{
private:
	QString	name;
	ParaMap	para_map;

public:
	Section(const QString& the_name) : name(the_name)	{		}


	~Section();

	const ParaMap *GetParasAvalable() const	{ return &para_map; }
	std::string	GetPara(int number, int which);

	int	GetNextParaNumber();

	bool	Add(int number, const QString& txt, const QString& fi_comment);
	bool	Delete(int number);
	bool	Update(int number, const QString& txt, const QString& fi_comment);

	void	Save(std::ofstream& file);
	void	WriteListing();
};

#endif

